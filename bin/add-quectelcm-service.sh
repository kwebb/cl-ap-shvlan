#!/bin/bash
set -ex

APNDNN=$1
SRCDIR=/var/tmp

if [ -f $SRCDIR/added-quectel-service ]; then
    echo "setup already ran; not running again"
    exit 0
fi

sudo cp /local/repository/bin/start-quectel-cm.sh /usr/local/bin/
sudo sed -i "s/APN/$APNDNN/" /usr/local/bin/start-quectel-cm.sh

sudo cp /local/repository/etc/quectel-cm.service /etc/systemd/system/
sudo systemctl daemon-reload
sudo systemctl start quectel-cm.service
sudo systemctl enable quectel-cm.service

touch $SRCDIR/added-quectel-service
