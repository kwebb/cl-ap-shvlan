"""This profile instantiates a set of APs and connects them (via shared VLAN) to an already-running (or soon-to-be running) Edge-proxy experiment.

Instructions:
Swap in.
"""

import geni.portal as portal
import geni.urn as urn
import geni.rspec.igext as ig
import geni.rspec.pg as pg
import geni.rspec.emulab as elab
import geni.rspec.emulab.pnext as pn

MATRIX_APS = ["cap1", "cap2"]
DENSE_APS = ["cap-wasatch", "cap-ustar", "cap-ebc"]
AP_DEVICES = MATRIX_APS + DENSE_APS
EDGE_DEVICES = ["ceg1",]
UE_NODES = ["nuc22",]
COTS_UE_IMG = "urn:publicid:IDN+emulab.net+image+PowderTeam:cots-focal-image"
SHVLANNAME = "CEGCAP"

# Parameters
pc = portal.Context()

# Set of AP devices to allocate
portal.context.defineStructParameter(
    "ap_devices", "Access Point Devices", [],
    multiValue=True,
    min=1,
    multiValueTitle="Access Point Devices.",
    members=[
        portal.Parameter(
            "id",
            "AP Device",
            portal.ParameterType.STRING,
            AP_DEVICES[0], AP_DEVICES,
            longDescription="AP radio devices to allocate/connect via RF matrix."
        ),
    ])

# Set of UE devices to allocate
portal.context.defineStructParameter(
    "ue_nodes", "UE Nodes", [],
    multiValue=True,
    min=0,
    multiValueTitle="UE Nodes.",
    members=[
        portal.Parameter(
            "id",
            "UE Node",
            portal.ParameterType.STRING,
            UE_NODES[0], UE_NODES,
            longDescription="NUC + COTS UE modem devices to allocate/connect via RF matrix."
        ),
    ])

pc.defineParameter("apn_name", "APN/DNN for COTS UE to use.",
                   portal.ParameterType.STRING, "celonadns",
                   longDescription="The Quectel connection manager will use this APN/DNN to obtain a PDU session.",
                   advanced=True)
pc.defineParameter("sharedVlanName","Shared VLAN Name",
                   portal.ParameterType.STRING,SHVLANNAME,
                   longDescription="This is the name of the shared VLAN that the separate AP experiments will attach to.",
                   advanced=True)

params = pc.bindParameters()
pc.verifyParameters()
request = pc.makeRequestRSpec()

# Create the "fronthaul LAN"
lan1 = request.LAN("lan1")
lan1.vlan_tagging = False
lan1.setNoBandwidthShaping()
lan1.connectSharedVlan(params.sharedVlanName)

# Request the AP devices
matrix_ap_nodes = []
for device in params.ap_devices:
    ap = request.RawPC(device.id)
    ap.component_id = device.id
    if device.id in MATRIX_APS:
        matrix_ap_nodes.append(ap)
        ap.Desire("rf-controlled", 1)
    lan1.addNode(ap)

# Request the nodes with the 5G module
ue_nodes = []
for idx,node in enumerate(params.ue_nodes):
    ue = request.RawPC("ue{}".format(idx+1))
    ue_nodes.append(ue)
    ue.component_id = node.id
    ue.disk_image = COTS_UE_IMG
    ue.Desire("rf-controlled", 1)
    ue.addService(pg.Execute(shell="sh", command="sudo cp /local/repository/etc/default.script /etc/udhcpc/"))
    ue.addService(pg.Execute(shell="bash", command="/local/repository/bin/add-quectelcm-service.sh {}".format(params.apn_name)))

# Set attenuation to max on all APs if more then one.
if len(matrix_ap_nodes) > 1:
    ue0 = ue_nodes[0]
    for ap in matrix_ap_nodes:
        ue0.addService(pg.Execute(shell="bash", command="/local/repository/bin/update-attens {} 95".format(ap.name)))
        pass
    pass

# Create the RF links between the COTS 5G modules and AP devices
idx = 1
for ap in matrix_ap_nodes:
    for ue in ue_nodes:
        rflink = request.RFLink("rflink{}".format(idx))
        rflink.addInterface(ap.addInterface("{}rf{}".format(ap.name,idx)))
        rflink.addInterface(ue.addInterface("{}rf{}".format(ue.name,idx)))
        idx += 1

# Print the RSpec to the enclosing page.
pc.printRequestRSpec()
